*** Variables ***

${product_card_locator}					xpath=//div[@class='card' and div[p[span[contains(text(),'{product_name}')]]]]
${product_detail_header}				id=product-detail

**** Keywords ***
Open selected product detail
	[Arguments]							${product_name}
	${replace_product_card_locator}		Replace string		${product_card_locator}		{product_name}		${product_name}
	common.Wait and click			${replace_product_card_locator}
