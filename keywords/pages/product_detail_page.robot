*** Variables ***

${add_to_cart_button_locator}			xpath=//button[contains(text(),'Add To Cart')]
${quantity_locator}						xpath=//input[@id='qty']
${plus_botton_locator}					xpath=//button[text()='+']

*** Keywords ***

Click add to card button
	common.Wait and click  ${add_to_cart_button_locator}

Click plus button
	SeleniumLibrary.Click button		${plus_botton_locator}
