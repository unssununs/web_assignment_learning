*** Variables ***

${no_results_locator}				xpath=//div[text()='No results found.']
${search_product name_locator}		xpath=//div[@class='card']//div[@class='card-content p-4']/p[contains(text(),{keyword})]
${search_result_locator}			xpath=//div[@class='column is-2']

**** Keywords ***

Verify no results found success
	SeleniumLibrary.Wait until element Is visible	${no_results_locator}

Verify search results is contains
	[Arguments]									${keyword}
	${replace_search_product name_locator}		Replace string		${search_product name_locator}		{keyword}		${keyword}
	${search_result}=							SeleniumLibrary.Get element count	${search_result_locator}
	FOR		${index}	IN RANGE				1					${search_result}+1
		SeleniumLibrary.Wait until element Is visible				${replace_search_product name_locator}[${$index}]
	END
