*** Variables ***

${Searc_bar_locator}				xpath=//input[@placeholder='Search for product']
${search_botton_locator}			id=searchbtn
${cart_count_locator}				xpath=//span[@id='lblCartCount']/b[text()='{cart_count}']
${clear_cart_button}				xpath=//button[text()='Clear All Cart']
${cart_icon_locator}				xpath=//i[contains(@class,'shopping-cart')]

*** Keywords ***

Input search bar
	[Arguments]						${search_word}
	common.Wait and input   		${Searc_bar_locator}  		${search_word}
	[Return]						${search_word}

Click search button
	Wait and click  ${search_botton_locator}

Cart count should be equal
	[Arguments]						${cart_count}
	${replace_cart_count_locator}		Replace string		${cart_count_locator}		{cart_count}		${cart_count}
	SeleniumLibrary.Wait until element Is visible				${replace_cart_count_locator}

Hover on cart icon
	SeleniumLibrary.Mouse over		${cart_count_locator}

Click clear cart
	SeleniumLibrary.Wait until element Is visible	${cart_icon_locator}
	SeleniumLibrary.Mouse over		${cart_icon_locator}
	SeleniumLibrary.Click button	${clear_cart_button}
