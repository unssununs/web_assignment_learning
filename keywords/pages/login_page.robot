*** Variables ***

${username_locator}		id=username
${password_locator}		id=password
${button_locator}		id=loginbtn
${home_logo_locator}	xpath=//img[@src='http://www.doppiotech.com/wp-content/uploads/2021/04/DoppioTech-logo-W-1024x354.png']

*** keywords ***

Input username
	[Arguments]			${username_value}
	common.Wait and input   ${username_locator}   ${username_value}

Input password
	[Arguments]			${password_value}
	common.Wait and input   ${password_locator}	   ${password_value}

Click login button
	common.Wait and click  ${button_locator}

Alert failed login should be present
	[Arguments]		${fail_massage}
	SeleniumLibrary.Alert should be present						${fail_massage}

Verify login success
	SeleniumLibrary.Wait until element is visible				${home_logo_locator}
