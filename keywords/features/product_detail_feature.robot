*** Keywords ***

Change quantity by pressing plus button to
	[Arguments]			${quantity}
	SeleniumLibrary.Input text					${quantity_locator}				0
	FOR		${index}	IN RANGE	${quantity}
		product_detail_page.Click plus button
	END
