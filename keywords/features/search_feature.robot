*** Keywords ***

Search for
	[Arguments]				${search_word}
	${search_word}= 		top_bar_page.Input search bar		${search_word}
	top_bar_page.Click search button
	[Return]				${search_word}
