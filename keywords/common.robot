*** Variables ***

${login_page_header}					xpath=//head/title[text()='Hello Doppio TECH!']

**** Keywords ***

Open website
	[Arguments]						${website}
	SeleniumLibrary.Open browser	${website}				browser=chrome
	SeleniumLibrary.Wait until page contains element		${login_page_header}

Wait and click
	[Arguments]			${target}
	SeleniumLibrary.Wait until element is visible			${target}
	SeleniumLibrary.Click element		${target}

Wait and input
	[Arguments]			${target}		${value}
	SeleniumLibrary.Wait until element is visible			${target}
	SeleniumLibrary.Input text			${target}			${value}
