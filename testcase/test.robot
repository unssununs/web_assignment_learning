*** Settings ***

Resource		${CURDIR}/../import.robot
Test Teardown   SeleniumLibrary.Close all browsers

*** Variables ***

${a}					12
${b}					12

*** Test Case ***

Testcase 03 - Verify that search function are working correctly

	${a}					Evaluate	${a}+${b}
	Log to Console			${a}${b}
