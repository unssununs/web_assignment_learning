*** Settings ***

Resource		${CURDIR}/../import.robot
Test Teardown	Run keywords
...				SeleniumLibrary.Close all browsers
...				Capture Page Screenshot

***Variables***

${web_url}						http://125.26.15.143:13132/
${invalid_username}				helloworld
${valid_username}				oz4899
${password}						1234
${valid_search_word}			CUP
${invalid_search_word}			abcd

*** Test Case ***

Testcase 01 - Verify that user cannot login with wrong username
	common.Open website			${web_url}
	login_feature.Login			${invalid_username}			${password}
	login_page.Alert failed login should be present			login failed

Testcase 02 - Verify that user can login to the system when username are correct
	common.Open website			${web_url}
	login_feature.Login			${valid_username}			${password}
	login_page.Verify login success

Testcase 03 - Verify that search function are working correctly
	common.Open website			${web_url}
	login_feature.Login			${valid_username}			${password}
	login_page.Verify login success
	Search for						${invalid_search_word}
	search_result_page.Verify no results found success
	search_feature.Search for		${valid_search_word}
	search_result_page.Verify search results is contains				${valid_search_word}

Testcase 04 - Verify that add to cart function are working correctly
	common.Open website				${web_url}
	login_feature.Login				${valid_username}			${password}
	login_page.Verify login success
	top_bar_page.Click clear cart
	home_page.Open selected product detail  		${valid_search_word}
	product_detail_page.Click add to card button
	top_bar_page.Cart count should be equal  		1
	product_detail_feature.Change quantity by pressing plus button to		5
	product_detail_page.Click add to card button
	top_bar_page.Cart count should be equal  		6
	top_bar_page.Click clear cart
	top_bar_page.Cart count should be equal  		0
