**** Settings ***

Library		SeleniumLibrary
Library		String

Resource	${CURDIR}/keywords/common.robot

Resource	${CURDIR}/keywords/pages/home_page.robot
Resource	${CURDIR}/keywords/pages/login_page.robot
Resource	${CURDIR}/keywords/pages/product_detail_page.robot
Resource	${CURDIR}/keywords/pages/search_result_page.robot
Resource	${CURDIR}/keywords/pages/top_bar_page.robot

Resource	${CURDIR}/keywords/features/login_feature.robot
Resource	${CURDIR}/keywords/features/product_detail_feature.robot
Resource	${CURDIR}/keywords/features/search_feature.robot
